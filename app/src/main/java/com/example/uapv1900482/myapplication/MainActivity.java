package com.example.uapv1900482.myapplication;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    BookDbHelper db;
    ListView Liste;
    TextView text1,text2;
    ArrayList<Book> lesBook;
    String title;
    String auteur;
    String year;
    String genre;
    String publisher;
    SimpleCursorAdapter cursor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();

                Intent intent = new Intent(view.getContext(), BookActivity.class);
                startActivityForResult(intent, 1);


            }
        });


        //**********************creer les variables *********************************

        text1=(TextView)findViewById(R.id.text1);
        text2=(TextView)findViewById(R.id.text2);
        db=new BookDbHelper(this);
        Liste=(ListView) findViewById(R.id.liste);
        registerForContextMenu(Liste);

        lesBook=this.liste();

       // db.populate();

         this.getAllData();










//*****************************clique sur la liste******************************************


        Liste.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent intent = new Intent(view.getContext(), BookActivity.class);
                 Book book=lesBook.get(position);
                  title=book.getTitle().toString();
                 auteur=book.getAuthors().toString();
                   year=book.getYear().toString();

                  genre=book.getGenres().toString();
                  publisher  =book.getPublisher().toString();


          intent.putExtra("title",title);
                intent.putExtra("auteur",auteur);
                intent.putExtra("year",year);
                intent.putExtra("genre",genre);
                intent.putExtra("publisher",publisher);
//                intent.putExtra("auteur",auteur);
                // Book b= dp.findBook(title);


                startActivityForResult(intent, 1);


            }
        });



    }
    //**************resultat apres modif ***************************

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode==1){
            if(resultCode==RESULT_OK){
              getAllData();
              cursor.notifyDataSetChanged();


            }
        }
    }


    //***********************menu **********************************************

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }






    //************************fonction qui recupére les données de la base de données*********************


    public void getAllData(){
        Cursor cur=db.fetchAllBooks();
        if(cur.getCount()==0){
            Toast.makeText(MainActivity.this,"0",Toast.LENGTH_LONG).show();
            return;
        }

        String [] valeurs=new String[]{
                db.COLUMN_BOOK_TITLE,
                db.COLUMN_AUTHORS
        };
        int[] item=new int[]{
                R.id.text1,
                R.id.text2




        };
        cursor=new SimpleCursorAdapter(this,R.layout.item,cur,valeurs,item,0);
        Liste=(ListView)findViewById(R.id.liste);
        Liste.setAdapter(cursor);



    }



//******************recuperer tout les elements de notre cursor dans une lise***********************

    public ArrayList<Book> liste() {
        String title, auteur, year, genre, publisher;
        Cursor cur = db.fetchAllBooks();
        Book book = null;
        ArrayList<Book> listeBook=new ArrayList<>();
        title = cur.getString(cur.getColumnIndex(db.COLUMN_BOOK_TITLE));
        auteur = cur.getString(cur.getColumnIndex(db.COLUMN_AUTHORS));
        year = cur.getString(cur.getColumnIndex(db.COLUMN_YEAR));
        genre = cur.getString(cur.getColumnIndex(db.COLUMN_GENRES));
        publisher = cur.getString(cur.getColumnIndex(db.COLUMN_PUBLISHER));


        book = new Book(title, auteur, year, genre, publisher);
        System.out.print(title);


        listeBook.add(book);
        while (cur.moveToNext()) {

            title = cur.getString(cur.getColumnIndex(db.COLUMN_BOOK_TITLE));
            auteur = cur.getString(cur.getColumnIndex(db.COLUMN_AUTHORS));
            year = cur.getString(cur.getColumnIndex(db.COLUMN_YEAR));
            genre = cur.getString(cur.getColumnIndex(db.COLUMN_GENRES));
            publisher = cur.getString(cur.getColumnIndex(db.COLUMN_PUBLISHER));


           book = new Book(title, auteur, year, genre, publisher);
           System.out.print(title);


            listeBook.add(book);

           }
           return listeBook;




    }




//********************menu contextuel *************************************


    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.menu1,menu);
    }


    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info= (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
       switch (item.getItemId()){
           case R.id.menu1:

               Liste=(ListView) findViewById(R.id.liste);
              Book book=lesBook.get(info.position);
              String title=book.getTitle().toString();
              db.deleteBook(title);
               Toast.makeText(MainActivity.this,"Delete"+info.position,Toast.LENGTH_LONG).show();
              this.getAllData();


               return true;
           default:
               return super.onContextItemSelected(item);


       }


    }
}
