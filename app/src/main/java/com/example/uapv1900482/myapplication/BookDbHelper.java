package com.example.uapv1900482.myapplication;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

import static android.database.sqlite.SQLiteDatabase.CONFLICT_IGNORE;

public class BookDbHelper extends SQLiteOpenHelper {

    private static final String TAG = BookDbHelper.class.getSimpleName();

    // If you change the database schema, you must increment the database version.
    private static final int DATABASE_VERSION = 1;

    public static final String DATABASE_NAME = "book.db";

    public static final String TABLE_NAME = "library";

    public static final String _ID = "_id";
    public static final String COLUMN_BOOK_TITLE = "title";
    public static final String COLUMN_AUTHORS = "authors";
    public static final String COLUMN_YEAR = "year";
    public static final String COLUMN_GENRES = "genres";
    public static final String COLUMN_PUBLISHER = "publisher";

    public BookDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        //this.getWritableDatabase();
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

	// db.execSQL() with the CREATE TABLE ... command
   db.execSQL(" create table "+TABLE_NAME+"(_id INTEGER PRIMARY KEY AUTOINCREMENT ,title TEXT,authors TEXT,year TEXT, genres TEXT,publisher TEXT)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }


   /**
     * Adds a new book
     * @return  true if the book was added to the table ; false otherwise
     */
    public boolean addBook(Book book) {
        SQLiteDatabase db = this.getWritableDatabase();


        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_BOOK_TITLE, book.getTitle());
        contentValues.put(COLUMN_AUTHORS,book.getAuthors());
        contentValues.put(COLUMN_YEAR,book.getYear());
        contentValues.put(COLUMN_GENRES,book.getGenres());
        contentValues.put(COLUMN_PUBLISHER,book.getPublisher());

        Long rowId= db.insert(TABLE_NAME, null, contentValues) ;






        // Inserting Row

	// call db.insert()
        db.close(); // Closing database connection

        return (rowId != -1);
    }



    /**
     * Updates the information of a book inside the data base
     * @return the number of updated rows
     */
    public int updateBook(Book b1,Book book) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values=new ContentValues();
        values.put(COLUMN_BOOK_TITLE,book.getTitle());
        values.put(COLUMN_AUTHORS,book.getAuthors());
        values.put(COLUMN_YEAR,book.getYear());
        values.put(COLUMN_GENRES,book.getGenres());
        values.put(COLUMN_PUBLISHER,book.getPublisher());
       int res= db.update(TABLE_NAME,values,COLUMN_BOOK_TITLE+" = ?",new String[]{b1.getTitle()});


        // updating row
	// call db.update()
        return res;
    }


    public Cursor fetchAllBooks() {
        SQLiteDatabase db = this.getReadableDatabase();
        String[] column=new String[]{_ID,COLUMN_BOOK_TITLE,COLUMN_AUTHORS,COLUMN_YEAR,COLUMN_GENRES,COLUMN_PUBLISHER};

        Cursor cursor=db.query(TABLE_NAME,column,null,null,null,null,null);
        // call db.query()

        if (cursor != null) {
            cursor.moveToFirst();
        }
        return cursor;
    }


    public void deleteBook(String title) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values=new ContentValues();
        db.delete(TABLE_NAME,COLUMN_BOOK_TITLE +"=?",new String[]{title});


        // call db.delete();
        db.close();
    }


    public Book findBook(String name){

        String query="select * from "+ TABLE_NAME +" where " + COLUMN_BOOK_TITLE +" = " + name;
        SQLiteDatabase db=this.getWritableDatabase();
        Cursor cursor=db.rawQuery(query,null);
        Book book=new Book();
        if(cursor.moveToFirst()){
            cursor.moveToFirst();
            book.setTitle(cursor.getString(1));
            book.setAuthors(cursor.getString(2));
            book.setYear(cursor.getString(3));
            book.setGenres(cursor.getString(4));
            book.setPublisher(cursor.getString(5));
        }else{
            book=null;
        }
        return book;
    }

    public void populate() {
        Log.d(TAG, "call populate()");
        addBook(new Book("Rouge Brésil", "J.-C. Rufin", "2003", "roman d'aventure, roman historique", "Gallimard"));
        addBook(new Book("Guerre et paix", "L. Tolstoï", "1865-1869", "roman historique", "Gallimard"));
        addBook(new Book("Fondation", "I. Asimov", "1957", "roman de science-fiction", "Hachette"));
        addBook(new Book("Du côté de chez Swan", "M. Proust", "1913", "roman", "Gallimard"));
        addBook(new Book("Le Comte de Monte-Cristo", "A. Dumas", "1844-1846", "roman-feuilleton", "Flammarion"));
        addBook(new Book("L'Iliade", "Homère", "8e siècle av. J.-C.", "roman classique", "L'École des loisirs"));
        addBook(new Book("Histoire de Babar, le petit éléphant", "J. de Brunhoff", "1931", "livre pour enfant", "Éditions du Jardin des modes"));
        addBook(new Book("Le Grand Pouvoir du Chninkel", "J. Van Hamme et G. Rosiński", "1988", "BD fantasy", "Casterman"));
        addBook(new Book("Astérix chez les Bretons", "R. Goscinny et A. Uderzo", "1967", "BD aventure", "Hachette"));
        addBook(new Book("Monster", "N. Urasawa", "1994-2001", "manga policier", "Kana Eds"));
        addBook(new Book("V pour Vendetta", "A. Moore et D. Lloyd", "1982-1990", "comics", "Hachette"));

        SQLiteDatabase db = this.getReadableDatabase();
        long numRows = DatabaseUtils.longForQuery(db, "SELECT COUNT(*) FROM "+TABLE_NAME, null);
        System.out.println(numRows);


        Log.d(TAG, "nb of rows="+numRows);
        db.close();
    }

    public static Book cursorToBook(Cursor cursor) {
        Book book = null;
	// build a Book object from cursor
        return book;
    }
}
