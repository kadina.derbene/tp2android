package com.example.uapv1900482.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class BookActivity extends AppCompatActivity {

    String title,auteur,year,genre,publisher;
    String title2,auteur2,year2,genre2,publisher2;
    EditText titre,auteur1,year1,genre1,publisher1;
    Button button;
    BookDbHelper db;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book);

      titre=(EditText)findViewById(R.id.nameBook);
        auteur1=(EditText)findViewById(R.id.editAuthors);
        year1=(EditText)findViewById(R.id.editYear);
       genre1=(EditText)findViewById(R.id.editGenres);
      publisher1=(EditText)findViewById(R.id.editPublisher);
      button=(Button)findViewById(R.id.button);

      db=new BookDbHelper(this);

        title=getIntent().getStringExtra("title");
        auteur=getIntent().getStringExtra("auteur");
        year=getIntent().getStringExtra("year");
        genre=getIntent().getStringExtra("genre");
        publisher=getIntent().getStringExtra("publisher");


        titre.setText(title);

        auteur1.setText(auteur);
        year1.setText(year);
        genre1.setText(genre);
        publisher1.setText(publisher);

      if(titre.getText().toString().equals("")&& auteur1.getText().toString().equals("") && year1.getText().toString().equals("")
              && genre1.getText().toString().equals("") && publisher1.getText().toString().equals("")) {
          Toast.makeText(BookActivity.this,"0",Toast.LENGTH_LONG).show();



          button.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {





                  title2 = titre.getText().toString();
                  auteur2 = auteur1.getText().toString();
                  year2 = year1.getText().toString();
                  genre2 = genre1.getText().toString();
                  publisher2 = publisher1.getText().toString();

                  db.addBook(new Book(title2,auteur2,year2,genre2,publisher2));




              Intent resultIntent=new Intent();
              resultIntent.putExtra("title",title2);
              resultIntent.putExtra("auteur",auteur2);
              setResult(RESULT_OK,resultIntent);
              finish();




          }
      });

        }else{








          button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Book b2=new Book(title,auteur,year,genre,publisher);

                    title2 = titre.getText().toString();
                    auteur2 = auteur1.getText().toString();
                    year2 = year1.getText().toString();
                    genre2 = genre1.getText().toString();
                    publisher2 = publisher1.getText().toString();

                    Book b1=new Book(title2,auteur2,year2,genre2,publisher2);
                    db.updateBook(b2,b1);



                    Intent resultIntent=new Intent();
                    resultIntent.putExtra("title",title2);
                    resultIntent.putExtra("auteur",auteur2);
                    resultIntent.putExtra("year",year2);
                    resultIntent.putExtra("genre",genre2);
                    resultIntent.putExtra("publisher",publisher2);
//                    setResult(RESULT_OK,resultIntent);
//                    finish();

                    Intent intent = new Intent(BookActivity.this, MainActivity.class);
                    startActivity(intent);





                }
            });







        }


    }
}
